# Memory Message Queue

An async message queue that lives in NodeJS's memory. Good for simulating
queues in development environments. You may wish to use in combination with
[iterator-worker](https://www.npmjs.com/package/iterator-worker).

Implements [abstract-message-queue](http://npmjs.com/abstract-message-queue).

## Usage

```javascript

import Queue from 'memory-message-queue';

const maxRetries = 4;

const queue = new Queue(maxRetries);

// Once the queue is constructed it is usable as specified by `abstract-message-queue`
setTimeout(()=>queue.send({ info: 'A message can be any object or string', exit: false }), 1000);
setTimeout(()=>queue.send({ info: 'An error has occured!', exit: true }), 2000);

// Wait for the queue to send and forward messages
for await(const message of queue) {
	console.log(message.info);

	if(message.exit === false) {
		// Remove message from the queue
		queue.delete();
	} else {
		// Messages will be re-added to the queue if `queue.delete` is not called

		// Loop will wait for the next message forever unless explicitly cancelled.
		break;
	}
}
```
