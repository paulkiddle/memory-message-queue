<a name="module_memory-message-queue"></a>

## memory-message-queue
Memory Message Queue


* [memory-message-queue](#module_memory-message-queue)
    * [module.exports](#exp_module_memory-message-queue--module.exports) ⏏
        * [new module.exports(maxRetries)](#new_module_memory-message-queue--module.exports_new)

<a name="exp_module_memory-message-queue--module.exports"></a>

### module.exports ⏏
An in-memory message queue. Reqeued messages are delivered instantly.

**Kind**: Exported class  
**See**: [abstract-message-queue](https://npmjs.com/package/abstract-message-queue) for usage details  
<a name="new_module_memory-message-queue--module.exports_new"></a>

#### new module.exports(maxRetries)
Create an instance of MemoryMessageQueue


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| maxRetries | <code>Number</code> | <code>2</code> | The number of times a message is retried before it is removed from the queue |

