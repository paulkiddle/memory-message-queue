import Queue from './index.js';
import { assertQueue } from 'abstract-message-queue';

const sleep = n => new Promise(r=>setTimeout(r,n));

const timeout = (p, t) => Promise.race([p, sleep(t).then(()=>Promise.reject('Timeout'))]);

test('Queue fits API', async ()=>{
	const queue = new Queue;
	await expect(assertQueue(queue, ()=>{})).resolves.not.toThrowError();
});

test('Drops messages after retry limit', async ()=>{
	const queue = new Queue(0);

	await queue.send('message');

	const i = queue[Symbol.asyncIterator]();

	await expect(timeout(i.next(), 100)).resolves.toEqual({ done: false, value: 'message' });
	await expect(timeout(i.next(), 100)).rejects.toEqual('Timeout');
});
