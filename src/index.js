/**
 * Memory Message Queue
 * @module memory-message-queue
 */

import { Queue } from 'async-fifo-queue';
import MessageQueue from 'abstract-message-queue';

/**
 * An in-memory message queue. Reqeued messages are delivered instantly.
 * @see {@link https://npmjs.com/package/abstract-message-queue|abstract-message-queue} for usage details
 */
export default class MemoryMessageQueue extends MessageQueue {
	#queue = new Queue
	#message

	/**
	 * Create an instance of MemoryMessageQueue
	 * @param {Number} maxRetries The number of times a message is retried before it is removed from the queue
	 */
	constructor(maxRetries=2){
		super({
			next: async () => {
				return this.#message = await this.#queue.get();
			},
			retry: async () => {
				const { message, retries } = this.#message;
				if(retries >= maxRetries) {
					return;
				}
				this.#send(message, retries + 1);
			},
			delete(){}
		});
	}

	#send(message, retries) {
		return this.#queue.put({ message, retries });
	}

	send(message){
		return this.#send(message, 0);
	}
}
